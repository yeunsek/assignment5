/*
 * Author: Eun Suk Lee
 * Date: 4/24/2015
 * Email:el24336@email.vccs.edu
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class ScoreGui extends JFrame implements ActionListener
{
	final int SIZE=7;//initialize constant
	JButton calculate = new JButton("Calculate");//make a button which has Calculate on it.
	JTextField []score = new JTextField[SIZE];//set size of textfield of score
	JTextField []weight = new JTextField[SIZE];//set size of textfield of weight
	JLabel name1 = new JLabel("Student's name");//make a label of student's name
	JTextField  name2 = new JTextField(20);//make a textfield for name
	JLabel [] moduleName = new JLabel[SIZE];//make a module name
	JPanel scorePanel = new JPanel();//make a panel for score textfield
	JPanel buttonPanel = new JPanel();//make a panel for calculate button
	JPanel namePanel = new JPanel();//panel for student's name
	
	double grade1;
	char grade2;
	static char letter;//letter grade
	static double average;//average grade
	double []scores = new double [SIZE];//this will be the score that is obtained from user input
	double []weights = new double [SIZE];//this will be the weights from user input
	
	/**
	 * Construct the graphic user interface for score calculator.
	 */
	public ScoreGui()
	{
		//set array for the each of module name.
		moduleName[1] = new JLabel("Assignments");
		moduleName[2] = new JLabel("Midterm Exam");
		moduleName[3] = new JLabel("Fianl Exam");
		moduleName[4] = new JLabel("Attendence");
		moduleName[5] = new JLabel("Weighted Average");
		moduleName[6] = new JLabel("Letter Grade");
		
		//make a layout for the score panel
		scorePanel.setBackground(Color.GREEN);
		scorePanel.setLayout (new GridLayout(7,3));
		//make a format of score panel
		for (int i=1; i<SIZE; i++)
		{
			scorePanel.add(moduleName[i]);
			score[i] = new JTextField(8);
			scorePanel.add(score[i]);
			weight[i] = new JTextField(8);
			scorePanel.add(weight[i]);
		}
		//make a layout for the button panel
		buttonPanel.setBackground(Color.YELLOW);
		buttonPanel.add(calculate);
		
		//make a layout for the name panel
		namePanel.add(name1);
		namePanel.add(name2);
		namePanel.setBackground(Color.CYAN);
		
		//add all panel to one frame
		add(namePanel, BorderLayout.PAGE_START);
		add(scorePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		
		//set action as user click the calculate
		calculate.addActionListener(this);
		
		//make frame visible and dimension
		setVisible(true);
		setSize(new Dimension(250,250));
		pack();
		setLocationRelativeTo(null);
	}
	/**
	 * calculate grade average scores
	 * @param score
	 * @param weight
	 * @return
	 */
	public static double calculateAvgScore (double []scores, double []weights)
	{
		double []product = new double [4];//set 4 products that is from scores and weights
		for (int i =0;i<4;i++)
		{
			product[i] = scores[i]*weights[i];
		}
		average=0;//get average by adding the products
		for (int j = 0;j<product.length;j++)
		{
			average += product[j];
		}
		return average;//return
	}
	public static char calculateLetterGrade(double a)//This is for the letter grade
	{
		//use if loop to set each letter grade according to average scores
		if (a>=90)
			letter = 'A';
		if (a<90 && a >=80)
			letter = 'B';
		if (a<80 && a >=70)
			letter = 'C';
		if (a<70 && a >=60)
			letter = 'D';
		if (a<60)
			letter = 'F';
		return letter;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		//set inputs as a double variable from string variable
		scores[0] = Double.parseDouble(score[1].getText());
		scores[1] = Double.parseDouble(score[2].getText());
		scores[2] = Double.parseDouble(score[3].getText());
		scores[3] = Double.parseDouble(score[4].getText());
		weights[0] = Double.parseDouble(weight[1].getText());
		weights[1] = Double.parseDouble(weight[2].getText());
		weights[2] = Double.parseDouble(weight[3].getText());
		weights[3] = Double.parseDouble(weight[4].getText());
		
		//get average and letter grade by using methods
		grade1 = Assignment5.calculateAvgScore(scores,weights);
		grade2 = Assignment5.calculateLetterGrade(average);
		
		//print output to the text field
		score[5].setText(String.valueOf(average));
		score[6].setText(String.valueOf(letter));
	}
}
